import requests
# import ipdb
import json
import datetime
from db_driver import get_document_store,STACKOVERFLOW_QUESTION_ANSWER
# from server import get_server,STACKOVERFLOWAPI
import requests
from flask import Flask, jsonify
from flask import request,Response

STACKOVERFLOWAPI = 'STACKOVERFLOWAPI'

class Server(object):
	def __init__(self,app_name):
		pass

	def run(self):
		raise Exception("please implement this method..")

class FlaskServerWrapper(Server):

	def __init__(self,app_name):
		self.app = Flask(app_name)

	def run(self):
		self.app.run(debug=True)


def get_server(label,app_name):
	"""
	using mongoclient here..
	"""
	if label == STACKOVERFLOWAPI:
		return FlaskServerWrapper(app_name)


questions = get_document_store(STACKOVERFLOW_QUESTION_ANSWER,'stackoverflow','questions')
answers = get_document_store(STACKOVERFLOW_QUESTION_ANSWER,'stackoverflow','answers')

app = Flask(__name__)


@app.route('/', methods=['GET'])
def get_welcome_page():
	return "Hello"

@app.route('/getitems/', methods=['GET'])
def get_tasks():
	output_list = {}
	args = request.args
	
	word = args.get('searchword')

	if not word:
		return Response(json.dumps({'Status':'error','Desc' : 'Please pass valid keyword'}),  mimetype='application/json')


	starttime = args.get('starttime')
	endtime = args.get('endtime')
	user_ids = args.getlist('users')
	# ipdb.set_trace()
	# if isinstance(user_ids,basestring):
	# 	user_ids = user_ids
	
	optional = False
	query_dict = {}

	if starttime or endtime or user_ids:
		optional = True

	# ipdb.set_trace()
	if starttime:
		starttime = datetime.datetime.strptime(starttime, "%Y%m%d")
		# query_dict['$gt'] =  starttime

	if endtime:
		endtime = datetime.datetime.strptime(endtime, "%Y%m%d")	
		# query_dict['$lt'] = endtime

	if user_ids:
		query_dict['user'] = { "$in" : user_ids}

	# query_dict = {}
	cur1 = questions.find(query_dict)
	cur2 = answers.find(query_dict)
	question_list = []
	for doc in cur1:
		print "in question.."
		if bool(user_ids):
			assert(doc['user'] in user_ids)
		doc_question_date = doc.get('question_date')
		if isinstance(doc_question_date,basestring):
			doc_question_date = doc_question_date.split(' ')[0]
			doc_question_date = datetime.datetime.strptime(doc_question_date, "%Y-%m-%d")
		if starttime and doc_question_date and (starttime > doc_question_date) :
			print "Skipping"
			continue

		if endtime and doc_question_date and endtime < doc_question_date:
			print "Skipping"
			continue
		# ipdb.set_trace()
		items = doc.get('data')
		if not items:
			continue
		else:
			tags = items.get('tags')
			if not tags:
				continue
			else:
				for tag in tags:
					if word in tag:
						del doc['_id']
						doc['question_date'] = str(doc_question_date)
						question_list.append(doc)
						break

	answer_list = []
	for doc in cur2:
		print "in answer.."
		if bool(user_ids):
			assert(doc['user'] in user_ids)
		doc_answer_date = doc.get('answer_date')
		if isinstance(doc_answer_date,basestring):
			doc_answer_date = doc_answer_date.split(' ')[0]
			doc_answer_date = datetime.datetime.strptime(doc_answer_date, "%Y-%m-%d")
		if starttime and  doc_answer_date and (starttime > doc_answer_date) :
			print "Skipping"
			continue

		if endtime and doc_answer_date and endtime < doc_answer_date:
			print "Skipping"
			continue
		items = doc.get('data')
		
		if not items:
			continue
		else:
			tags = items.get('tags')
			if not tags:
				continue
			else:
				for tag in tags:
					if word in tag:
						# ipdb.set_trace()
						del doc['_id']
						doc['answer_date'] = str(doc['answer_date'])
						answer_list.append(doc)
						break
	
	output_list['questions'] = question_list
	output_list['answers'] = answer_list
	output_list['count'] = len(question_list) + len(answer_list)
	return Response(json.dumps(output_list),  mimetype='application/json')
		

if __name__ == '__main__':
	# print get_tasks()
	app.run(debug=True)