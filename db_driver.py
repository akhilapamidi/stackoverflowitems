from pymongo import MongoClient
from pymongo import Connection

STACKOVERFLOW_QUESTION_ANSWER = 'STACKOVERFLOW_QUESTION_ANSWER'
DB_LABEL_LIST = [STACKOVERFLOW_QUESTION_ANSWER]

class MongoWrapper(object):
	def __init__(self, db_name, collection_name):
		self.cl = MongoClient()
		self.client =  self.cl[db_name][collection_name]

	def get_doc_client(self,db_name,collection_name):
		return self.client		

	def insert(self,doc):
		self.client.insert(doc)

	def update(self,doc,filter):
		self.client.update(filter, {"$set": doc}, upsert=True)

	def find(self,query):
		if not query:
			return self.client.find({})
		else:
			return self.client.find(query)


def get_mongo_client(name, collection_name):
	return MongoWrapper(name, collection_name)

def get_document_store(label, name, collection_name):
	"""
	using mongoclient here..
	"""
	if label == STACKOVERFLOW_QUESTION_ANSWER:
		return get_mongo_client(name, collection_name)