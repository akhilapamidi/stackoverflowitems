from pymongo import MongoClient
from pymongo import Connection
import ipdb

cl = MongoClient()
answers = cl["stackoverflow"]["answers"]

cur = answers.find()

for doc in cur:
	print "hello"
	answer_id = doc['answer_id']
	question_id = doc.get('data').get('question_id')

	if not answer_id:
		continue

	answer_link = 'http://stackoverflow.com/a/' + str(answer_id)

	if question_id:
		question_link = 'http://stackoverflow.com/q/' + str(question_id)
		doc['question_link'] = question_link

	doc['answer_link'] = answer_link

	answers.update({'answer_id':answer_id}, {"$set": doc}, upsert=False)