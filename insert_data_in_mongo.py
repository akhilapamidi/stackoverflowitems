import ipdb
from datetime import datetime
import pytz
from db_driver import get_document_store,STACKOVERFLOW_QUESTION_ANSWER
from utils import get_date_from_timestamp
import requests

questions_client = get_document_store(STACKOVERFLOW_QUESTION_ANSWER,'stackoverflow','questions')
answers_client = get_document_store(STACKOVERFLOW_QUESTION_ANSWER,'stackoverflow','answers')


QUESTIONS_URL_1 = 'https://api.stackexchange.com/2.2/users/'
QUESTIONS_URL_2 = '/values?order=desc&site=stackoverflow'
ANSWERS_URL_2 = '/answers?order=desc&site=stackoverflow'


users_list = ['100297']

# users_list = ['100297','22656','95810','846892','429','19082','893','4172','23354','61974']

QUESTION_LABEL = 'Question'
ANSWER_LABEL = 'Answer'


def dump_stackoverflow_data_in_dbclient(label, url):
	data = requests.get(q_url).json()

	values = data.get('items', [])

	if not values:
		values = []
		print "no values..skipping"

	for value in values:
		if label == ANSWER_LABEL:
			value_id = answer.get('answer_id')
			value_url = 'https://api.stackexchange.com/2.2/answers/' + str(answer_id) + '/values?order=desc&sort=activity&site=stackoverflow'
			value_data =requests.get(value_url).json()
		# ipdb.set_trace()
		try:
			tags = value_data.get('items')[0].get('tags')
		except:
			ipdb.set_trace()

		if tags:
			value['tags'] = tags
		
		value_id = value.get('value_id')
		
		if not value_id:
			print " no value id here in this data.."
			continue

		link = value.get('owner',{}).get('link')
		display_name = value.get('owner', {}).get('display_name')

		q_date = value.get('creation_date') 
		if value.get('last_edit_date'):
			q_date = value.get('last_edit_date')
		q_date = get_date_from_timestamp(q_date)
		output = {'user': user, 'value_id':value_id, 'link':link, 'display_name':display_name, 'data':value, 'value_date':q_date}

		#insert only if does not exists
		db_doc = values_client.find({'value_id' : value_id})
		if db_doc.count() == 0:
			values_client.insert(output)
		else:
			values_client.update({'value_id':value_id}, {"$set": output}, upsert=True)




for user in users_list:
	print "Inserting : " , user
	q_url = QUESTIONS_URL_1 + user + QUESTIONS_URL_2
	a_url = QUESTIONS_URL_1 + user + ANSWERS_URL_2

	print "getting values.."
	dump_stackoverflow_data_in_dbclient(QUESTION_LABEL,q_url)
	dump_stackoverflow_data_in_dbclient(ANSWER_LABEL,a_url)