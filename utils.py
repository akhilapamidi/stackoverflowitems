def get_date_from_timestamp(ts):
	your_timestamp = ts
	dt_naive_utc = datetime.utcfromtimestamp(your_timestamp)
	return dt_naive_utc.replace(tzinfo=pytz.utc)