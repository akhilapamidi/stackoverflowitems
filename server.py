import requests
from flask import Flask, jsonify
from flask import request,Response

STACKOVERFLOWAPI = 'STACKOVERFLOWAPI'

class Server(object):
	def __init__(self,app_name):
		pass

	def run(self):
		raise Exception("please implement this method..")

class FlaskServerWrapper(Server):

	def __init__(self,app_name):
		self.app = Flask(app_name)

	def run(self):
		self.app.run(debug=True)


def get_server(label,app_name):
	"""
	using mongoclient here..
	"""
	if label == STACKOVERFLOWAPI:
		return FlaskServerWrapper(app_name)